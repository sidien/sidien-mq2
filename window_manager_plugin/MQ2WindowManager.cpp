#include <mq/Plugin.h>

PLUGIN_VERSION(0.1);
PreSetup("MQ2WindowManager");


void WindowCommand(SPAWNINFO *pChar, char *szLine) {
    if (GetGameState() != GAMESTATE_INGAME) {
        WriteChatf("Was asked to do '%s', but am not in game", szLine);
        return;
    }

    CSidlScreenWnd* pWnd = (CSidlScreenWnd*)FindMQ2Window("HotButtonWnd3");

    auto location = pWnd->GetLocation();
    WriteChatf("Location: top: %d, left %d", location.top, location.left);
    CXPoint myPos(0, 1177);
    pWnd->Resize(507, 52);
    //pWnd->Move(topLeft);
    pWnd->Location.left = -6;
    pWnd->SetNeedsSaving(true);

    pWnd->StoreIniInfo();

}

// Called once, when the plugin is to initialize
PLUGIN_API VOID InitializePlugin(VOID) {
    DebugSpewAlways("Initializing MQ2WindowManager");
    AddCommand("/windowtest", WindowCommand);
}

// Called once, when the plugin is to shutdown
PLUGIN_API VOID ShutdownPlugin(VOID) {
    DebugSpewAlways("Shutting down MQ2WindowManager");
    RemoveCommand("/windowtest");
}

