#pragma once
#include <string>
#include <map>

struct WindowLocation {
    int x;
    int y;
};

struct WindowSize {
    int height;
    int width;
};

struct WindowLayout {
    std::string windowId;
    WindowLocation location;
    WindowSize size;
    bool visible;
};

struct LayoutManagerConfig {
    std::string configFileVersion;
    std::map<std::string, std::map<std::string, WindowLayout>> layouts;
};
