#include <zyre.h>
#include <iostream>
#include <csignal>
#include <map>
#include <iterator>

#define TERMINATE_COMMAND "$TERM"

zyre_t *node;
zactor_t *actor;

void cleanupAndExit() {
    zstr_sendx(actor, TERMINATE_COMMAND, NULL);
    zactor_destroy(&actor);

    zyre_stop(node);
    // wait for node to stop
    zclock_sleep(100);
    zyre_destroy(&node);
    exit(0);
}

std::map<std::string, std::string> peerNamesByUUID;

void signalHandler(int signal) {
    cleanupAndExit();
}

void receiveLoop(zsock_t *pipe, void *args) {
    zyre_t *node = (zyre_t *) (args);

    zsock_signal(pipe, 0);
    bool terminated = false;

    zpoller_t *poller = zpoller_new(pipe, zyre_socket(node), NULL);

    while (!terminated) {

        void *which = zpoller_wait(poller, -1); // no timeout
        if (which == pipe) {
            zmsg_t *msg = zmsg_recv(which);
            if (!msg) {
                break;
            }

            char *command = zmsg_popstr(msg);

            if (streq (command, TERMINATE_COMMAND)) {
                terminated = true;
            }
        } else if (which == zyre_socket(node)) {
            zmsg_t *msg = zmsg_recv(which);
            char *event = zmsg_popstr(msg);
            char *peer = zmsg_popstr(msg);
            char *name = zmsg_popstr(msg);
            char *group_or_message = zmsg_popstr(msg);
            char *message = zmsg_popstr(msg);
            peerNamesByUUID[std::string(peer)] = std::string(name);
            if (streq(event, "SHOUT")) {
                std::cout << "[shout:" << group_or_message << "]["
                          << std::string(name) << "]" << std::endl
                          << message << std::endl;
                std::cout << std::endl;
            } else if (streq(event, "WHISPER")) {
                std::cout << "[whisper][" << std::string(name) << "]" << std::endl
                          << group_or_message << std::endl;
            }
            free(event);
            free(peer);
            free(name);
            free(group_or_message);
            free(message);
            zmsg_destroy(&msg);
        }
    }
    zpoller_destroy(&poller);
}

int main(int argc, char *argv[]) {
    zyre_t *node = zyre_new("dannetcat");
    if (!node) {
        return 1;
    }

    zyre_start(node);
    zclock_sleep(50);
    actor = zactor_new(receiveLoop, node);
    std::signal(SIGINT, signalHandler);

    zlist_t *peers = zyre_peers(node);

    for (int i = 0; i < zlist_size(peers); i++) {
        char *peer_uuid = (char *) zlist_pop(peers);
        auto it = peerNamesByUUID.find(peer_uuid);
        std::cout << peer_uuid;
        if (it != peerNamesByUUID.end()) {
            std::cout << " (" << it->second << ")";
        }

        std::cout << std::endl;
        free(peer_uuid);
    }
    std::cout << zyre_uuid(node) << " (" << zyre_name(node) << ") [this node]";
    zlist_destroy(&peers);

    cleanupAndExit();
    return 0;
}
