#include <winsock2.h>
#include <Ws2tcpip.h>
#include <iostream>
#include <chrono>
#include <thread>

#define CMD_MSGALL "\tSMSGALL\n"
const char *SEND_LINE_TERM = "\n";

void logConnectionError(int result);

int main(int argc, char const *argv[]) {
    if (argc == 1) {
        std::cout << "Need a message to pass";
        return 1;
    }
    const char *msg = argv[1];

    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
        std::cout << "WSAStartup failed. Error code: " << WSAGetLastError() << std::endl;
        return 1;
    }

    SOCKET sock;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cout << "Socket creation error code: " << WSAGetLastError() << std::endl;
        return -1;
    }

    struct sockaddr_in eqbc_addr{};
    eqbc_addr.sin_family = AF_INET;
    eqbc_addr.sin_port = htons(2112);
    if (inet_pton(AF_INET, "127.0.0.1", &eqbc_addr.sin_addr) <= 0) {
        std::cout << "Invalid address, or address not supported " << std::endl;
        return -1;
    }

    if (connect(sock, (struct sockaddr *) &eqbc_addr, sizeof(eqbc_addr)) < 0) {
        std::cout << "Connection Failed" << std::endl;
        return -1;
    }

    char buffer[1024] = {0};
    int result;

    const char *login = "LOGIN=eqbccat;";
    result = send(sock, login, strlen(login), 0);
    if (result == SOCKET_ERROR || (result == 0 && WSAGetLastError() != WSAEWOULDBLOCK)) {
        logConnectionError(result);
        return EXIT_FAILURE;
    }
    recv(sock, buffer, 1024, 0);
    std::cout << "Read: " << buffer << std::endl;

    std::cout << std::endl << "BCAA: " << msg << std::endl;
    send(sock, CMD_MSGALL, strlen(CMD_MSGALL), 0);
    send(sock, msg, strlen(msg), 0);
    result = send(sock, SEND_LINE_TERM, strlen(SEND_LINE_TERM), 0);
    if (result == SOCKET_ERROR || (result == 0 && WSAGetLastError() != WSAEWOULDBLOCK)) {
        logConnectionError(result);
        return EXIT_FAILURE;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    shutdown(sock, SD_RECEIVE);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    closesocket(sock);
    WSACleanup();
}

void logConnectionError(int result) {
    std::cout << "[eqbccat] ERROR logging in to server" << std::endl
              << "Result: " << result << std::endl
              << "WSAGetLastError(): " << WSAGetLastError() << std::endl;
}
