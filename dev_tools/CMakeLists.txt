PROJECT(sid_dev_tools)

find_package("zeromq")
find_package("czmq")
find_package("zyre")

add_executable(eqbccat
        eqbccat.cpp)
target_link_libraries(eqbccat
        wsock32
        ws2_32)


add_executable(dannetcat
        dannetcat.cpp)
target_link_libraries(dannetcat
        zyre)
