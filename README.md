# sidien-mq2

This repository contains some MacroQuest2 plugins and tools. Except for
MQ2Boxr, they should be considered sandbox projects or development tools
for MQ2.
 
## MQ2Boxr

Provides a command to control boxes running CWTN plugins, KissAssist, and
rgmercs. The idea is to have a common command, which can be broadcast to all 
boxed characters, regardless of what they are running (so long as it is any
of the 3 mentioned above).

### Commands

| Command                         | Description
|---------------------------------|-------------
| `/boxr Pause`                   | Pause the character 
| `/boxr Unpause`                 | Unpause the character
| `/boxr Camp`                    | Camp in one spot, assist the MA within camp radius, and return to camp after battle
| `/boxr Chase`                   | Chase the MA, and assist in battle
| `/boxr Manual`                  | Do not chase, do not return to camp. This state behaves a little bit different between different boxes, see [specifics](#specifics) below
| `/boxr CampRadius <N>`          | Sets camp radius to `N`
| `/boxr RaidAssistNum <1\|2\|3>` | Toggles which Raid MA the character will assist
| `/boxr Debug <on\|off>`         | Turn on/off debug logging
| `/boxr Help`                    | Print help text

### Specifics

The following sections document how each boxr command maps to the different
plugins/macros.

#### CWTN plugins

The mapping to CWTN commands is very straight-forward

| Boxr command         | Action
|----------------------|-------------
| `Camp`               | `/war mode assist` 
| `Chase`              | `/war mode chase`
| `Manual`             | `/war mode manual`
| `CampRadius <N>`     | `/war campradius N`
| `RaidAssistNum <N>`  | `/war RaidAssistNum` 

#### KissAssist

| Boxr command         | Action
|----------------------|-------------
| `Camp`               | `/camphere on`
| `Chase`              | `/chaseon`
| `Manual`             | `/chaseoff` <br/> `/camphere off`
| `CampRadius <N>`     | `/campradius N`
| `RaidAssistNum <N>`  | `/switchma <Name of Raid MA #N> tank 1`

**N.B.** The `RaidAssistNum` uses arguments to the `/switchma` command, which I 
believe should be considered internal to KissAssist (indeed, the `/switchma` 
command itself is not documented, and I think its the purpose is for the 
group to switch MA in case the primary MA dies).

#### rgmercs

| Boxr command         | Action
|----------------------|-------------
| `Camp`               | `/rg camphard`
| `Chase`              | `/rg chaseon`
| `Manual`             | `/rg chaseoff`<br/>`/rg campoff`
| `CampRadius <N>`     | `/rg autocampradius <N>`
| `RaidAssistNum <N>`  | `/rg AssistOutside 1`<br />`/rg OutsideAssistList <Name of Raid MA #N>`

**N.B.** The rgmercs adapter has undergone much less testing than the
CWTN and KissAssist ones.

## MQ2SidDevTools

This plugin provides some commands that are intended to assist during
development of MQ2 plugins.

# MQ2 development tools

## eqbccat 

This is a small utility that allows sending commands to EQ clients,
over EQBC, from the command line. It is used by the CMake scripts to
unload/reload plugins during installation. See one of the plugin 
[CMakeLists.txt](boxr_plugin/CMakeLists.txt#:~:text=install%28CODE,%2F%2Fdevunload%20MQ2Boxr%5C%22%29%22%29)
for an example of usage.

# Building

The CMake script expects the environment variable `MQ2_SRC_DIR` to be set to
the base directory of the MacroQuest2 sources. I have only tested this with
[the Very Vanilla](https://gitlab.com/redguides/VeryVanilla) sources.

## Installing the plugins to a running MQ2

If you provide a path to your MQ2 installation in the `MQ2_DIR` variable,
the build script will install the built plugins, and make sure they are 
reloaded. Before installing, the install scripts sends EQBC commands to all 
attached clients to unload the `MQ2SidDevTools` plugin, and to load it once
it has been installed.

For the other plugins, the install script uses `MQ2SidDevTools` to unload and
reload the plugins only on attached clients that have them loaded.

## Building from command line

To build using CMake, you need Microsoft Visual Studio. Open `Developer
Command Prompt for VS <version>`. Example of building all plugins:

```shell
cd source/sidien-mq2
mkdir build
cd build
cmake -D MQ2_SRC_DIR=c:/users/sidien/src/VeryVanilla -D MQ2_DIR=c:/users/sidien/mq2/Live -A Win32 .. 
msbuild sidien-mq2.sln -p:Configuration=Release -p:OutDir=../Release/
```

The built plugins can then be found in `build/Release`.

## Compiling MQ2Main from command line

If you do not have MQ2 sources built, you will need to build at least MQ2Main
first. The script `scripts/build_deps.cmd` will clone the VeryVanilla repo
(or update it to `HEAD` on `master`, if it is already cloned), and compile
the `MQ2Main` subproject. Run the script, and then follow 
[the instructions above](#building-from-command-line) with the option 
`-DMQ2_SRC_DIR=c:/users/sidien/src/sidien-mq2/deps/VeryVanilla` (of course
adjusting the path to where you actually have the code checked out).
