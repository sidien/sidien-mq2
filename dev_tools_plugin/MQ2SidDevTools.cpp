#include <mq/Plugin.h>

PreSetup("MQ2CPUAffinityHelper");
PLUGIN_VERSION(0.1);

#include <unordered_set>
#include "Logger.h"
#include "sid_common.h"

std::unordered_set<std::string> pluginsToLoad;
Logger logger("MQ2SidDevTools");
bool debug = false;

void DevunloadCommand([[maybe_unused]] SPAWNINFO *pChar, char *szLine) {
    if (!isPluginLoaded(szLine)) {
        logger.debugf("%s not loaded", szLine);
        return;
    }
    logger.debugf("%s is loaded, will unload", szLine);
    pluginsToLoad.insert(szLine);
    runCommandf("/plugin %s unload noauto", szLine);
}

void reloadPlugin(const std::string &plugin) {
    logger.debugf("Reloading %s", plugin.c_str());
    pluginsToLoad.erase(plugin);
    runCommandf("/plugin %s load noauto", plugin.c_str());
}

void DevreloadCommand([[maybe_unused]] SPAWNINFO *pChar, [[maybe_unused]] char *szLine) {
    if (pluginsToLoad.find(szLine) == pluginsToLoad.end()) {
        logger.debugf("%s was not loaded before, so not loading it now", szLine);
        return;
    }
    reloadPlugin(szLine);
}

void DevreloadAllCommand([[maybe_unused]] SPAWNINFO *pChar, [[maybe_unused]] char *szLine) {
    logger.debugf("Reloading %d unloaded plugin(s)", pluginsToLoad.size());
    for (const auto &plugin : pluginsToLoad) {
        reloadPlugin(plugin);
    }
}

void DevdebugCommand([[maybe_unused]] SPAWNINFO *pChar, [[maybe_unused]] char *szLine) {
    debug = !debug;
    if (debug) {
        logger.infof("Enabled debug logging");
    } else {
        logger.infof("Disabled debug logging");
    }
    logger.setDebugEnabled(debug);
}

// Called once, when the plugin is to initialize
PLUGIN_API VOID InitializePlugin(VOID) {
    DebugSpewAlways((PCHAR) "Initializing MQ2SidDevTools");
    logger.setDebugEnabled(debug);
    logger.infof("Plugin loaded");
    AddCommand((PCHAR) "/devunload", DevunloadCommand);
    AddCommand((PCHAR) "/devreload", DevreloadCommand);
    AddCommand((PCHAR) "/devdebug", DevdebugCommand);
    AddCommand((PCHAR) "/devreloadall", DevreloadAllCommand);
}

// Called once, when the plugin is to shutdown
PLUGIN_API VOID ShutdownPlugin(VOID) {
    DebugSpewAlways((PCHAR) "Shutting down MQ2SidDevTools");
    logger.infof("Plugin shutdown");
    RemoveCommand((PCHAR) "/devunload");
    RemoveCommand((PCHAR) "/devreload");
    RemoveCommand((PCHAR) "/devdebug");
    RemoveCommand((PCHAR) "/devreloadall");
}
