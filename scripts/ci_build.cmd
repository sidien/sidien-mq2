@echo off
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars32.bat"
call "%CI_PROJECT_DIR%\scripts\build_deps.cmd"
mkdir "%CI_PROJECT_DIR%\build"
cd "%CI_PROJECT_DIR%\build"
cmake -D MQNEXT_SRC_DIR=%CI_PROJECT_DIR%/deps/mqnext -A Win32 ..
SET Platform=
tools/build_scripts/vcpkg_mq.ps1
msbuild sidien-mq2.sln -p:Configuration=Release -p:OutDir="%CI_PROJECT_DIR%/build/Release/"
