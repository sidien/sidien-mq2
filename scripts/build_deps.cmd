@echo off
IF NOT EXIST %~dp0..\deps (mkdir deps)
cd %~dp0\..\deps
IF NOT EXIST mqnext (
    echo Cloning mqnext repo
    git clone --single-branch --no-tags https://gitlab.com/macroquest/next/mqnext/
    cd mqnext
    git submodule init
) ELSE (
    echo Cleaning up mqnext repo
    cd mqnext
    git clean -fxd
    git fetch
    git reset --hard origin/master
)

git submodule update
.\MQ2Auth.exe /silent

cd src
msbuild -p:Configuration=Release -p:PlatformTarget=Win32 -p:Platform=Win32

cd ../plugins
git clone --branch mqnext --single-branch --no-tags https://github.com/brainiac/MQ2Nav/
cd MQ2Nav
msbuild -p:Configuration=Release -p:PlatformTarget=Win32 -p:Platform=Win32
