#pragma once

#include <memory>

#include "sid_assist_core.h"
#include "navigation.h"

class SidAssist {
public:
    static SidAssist &getInstance() {
        static SidAssist instance;
        return instance;
    }

    SidAssist(SidAssist const &) = delete;
    void operator=(SidAssist const &) = delete;

    void OnPulse();
    void LoadConfig();

    // Commands
    void SetNavigationCamp(float x, float y, float z);
    void SetNavigationManual();
    void SetNavigationChase();

private:
    SidAssist() = default;

    std::unique_ptr<Task> currentTask;
};
