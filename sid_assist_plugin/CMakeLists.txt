Project(MQ2SidAssist)

find_package("yaml-cpp" REQUIRED)
find_package("fmt" REQUIRED)

include_directories(.
        ../common
        ${MQNEXT_INCLUDE_DIRS})

add_library(MQ2SidAssist SHARED
        $<TARGET_OBJECTS:sid_common_objlib>
        sid_assist_core.cpp sid_assist_core.h
        sid_assist.cpp sid_assist.h
        navigation.cpp navigation.h
        MQ2SidAssist.cpp)

target_link_libraries(MQ2SidAssist PRIVATE
        ${MQNEXT_LIBS}
        yaml-cpp
        fmt::fmt-header-only)

if (DEFINED MQNEXT_DIR)
    install(CODE "execute_process(COMMAND \"${CMAKE_BINARY_DIR}/dev_tools/eqbccat${CMAKE_EXECUTABLE_SUFFIX}\" \"//devunload MQ2SidAssist\")")
    install(TARGETS MQ2SidAssist RUNTIME DESTINATION "${MQNEXT_DIR}/plugins" COMPONENT Runtime)
    install(CODE "execute_process(COMMAND \"${CMAKE_BINARY_DIR}/dev_tools/eqbccat${CMAKE_EXECUTABLE_SUFFIX}\" \"//devreload MQ2SidAssist\")")
endif ()
