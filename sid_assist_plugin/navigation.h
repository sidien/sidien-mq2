#pragma once

#include <cstdlib>
#include <memory>
#include <mq/Plugin.h>

#include "sid_assist_core.h"

enum NavigationMode {
    manual,
    camp,
    chase
};

struct Loc {
    Loc(float x, float y, float z);
    [[nodiscard]] double distanceTo(const std::shared_ptr<Loc> &that) const;
    float x, y, z;
};

class ReturnToCampTask : public Task {
public:
    [[nodiscard]] SidTaskType Type() const override { return navigation; };
    [[nodiscard]] bool IsDone() const override;
    void DoWork() override;
    void Cleanup() override;
};

class NavigateToSpawnTask : public Task {
public:
    explicit NavigateToSpawnTask(PSPAWNINFO target);

    [[nodiscard]] SidTaskType Type() const override { return navigation; };
    [[nodiscard]] bool IsDone() const override;
    void DoWork() override;
    void Cleanup() override;
private:
    PSPAWNINFO target;
};

class Navigator {
public:
    static Navigator &GetInstance() {
        static Navigator instance;
        return instance;
    }

    Navigator(Navigator const &) = delete;
    void operator=(Navigator const &) = delete;

    std::unique_ptr<Task> GetTask();

    void SetCamp(float x, float y, float z);
    void SetModeManual();

    [[nodiscard]] double GetDistanceToCamp() const;
    [[nodiscard]] static double GetDistanceToChaseTarget();
    void ReturnToCamp() const;

    void SetModeChase();

private:
    Navigator() = default;
    NavigationMode mode = manual;
    std::unique_ptr<Loc> campLocation;
};
