#pragma once

#include "Logger.h"

extern Logger logger;

enum BoxState {
    idle,
    travelling
};

enum SidTaskType {
    none,
    navigation
};

class Task {
public:
    [[nodiscard]] virtual SidTaskType Type() const = 0;
    virtual void Prepare() {};
    [[nodiscard]] virtual bool IsDone() const { return true; };

    virtual void Cleanup() {};
    virtual void DoWork() = 0;

    virtual void Abort() {};
    virtual ~Task() = default;
};

struct Feature {
    virtual const char* GetName() = 0;

    virtual bool HasWork() = 0;
    virtual void DoWork() = 0;
};
