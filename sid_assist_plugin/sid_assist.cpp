#include "sid_assist.h"


void SidAssist::SetNavigationCamp(float x, float y, float z) {
    Navigator::GetInstance().SetCamp(x, y, z);
    if (currentTask && currentTask->Type() == navigation) {
        currentTask->Abort();
        currentTask->Cleanup();
        currentTask.reset();
    }
}

void SidAssist::SetNavigationManual() {
    Navigator::GetInstance().SetModeManual();
    if (currentTask && currentTask->Type() == navigation) {
        currentTask->Abort();
        currentTask->Cleanup();
        currentTask.reset();
    }
}

void SidAssist::SetNavigationChase() {
    Navigator::GetInstance().SetModeChase();
    if (currentTask && currentTask->Type() == navigation) {
        currentTask->Abort();
        currentTask->Cleanup();
        currentTask.reset();
    }
}

void SidAssist::OnPulse() {
    SidTaskType currentTaskType = (currentTask != nullptr) ? currentTask->Type() : none;

    if (currentTaskType < navigation) {
        auto newTask = Navigator::GetInstance().GetTask();
        if (newTask != nullptr) {
            if (currentTask != nullptr) {
                currentTask->Abort();
                currentTask->Cleanup();
            }
            currentTask = std::move(newTask);
        }
    }

    if (currentTask == nullptr) {
        return;
    }

    currentTask->DoWork();
    if (currentTask->IsDone()) {
        currentTask->Cleanup();
        currentTask.reset();
    }
}

void SidAssist::LoadConfig() {
}
