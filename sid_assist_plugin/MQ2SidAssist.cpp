#include <mq/Plugin.h>
#include <chrono>
#include <mq/utils/Args.h>

#include "sid_assist.h"

PreSetup("MQ2SidAssist");
PLUGIN_VERSION(0.1);

using MQ2Args = Args<&WriteChatf>;
using MQ2HelpArgument = HelpArgument;

void SidAssistCommand(SPAWNINFO *pChar, char *Buffer) {
    MQ2Args arg_parser("MQ2SidAssist");
    arg_parser.Prog("/sid");

    args::Group commands(arg_parser, "", args::Group::Validators::AtMostOne);

    args::Command reload(commands, "reload", "reload character configuration",
                         [](args::Subparser &parser) {
                             args::Group arguments(parser, "", args::Group::Validators::DontCare);
                             MQ2HelpArgument h(arguments);
                             parser.Parse();
                             SidAssist::getInstance().LoadConfig();
                         });

    MQ2HelpArgument h(commands);

    auto args = allocate_args(Buffer);
    try {
        arg_parser.ParseArgs(args);
    } catch (const args::Error &e) {
        WriteChatColor(e.what());
    }

    if (args.empty()) {
        arg_parser.Help();
    }
}


/**
 * @fn InitializePlugin
 *
 * This is called once on plugin initialization and can be considered the startup
 * routine for the plugin.
 */
PLUGIN_API void InitializePlugin() {
    DebugSpewAlways("MQ2SidAssist::Initializing version %f", MQ2Version);
    WriteChatf("SidAssist");
    AddCommand("/sid", SidAssistCommand);
}

/**
 * @fn ShutdownPlugin
 *
 * This is called once when the plugin has been asked to shutdown.  The plugin has
 * not actually shut down until this completes.
 */
PLUGIN_API void ShutdownPlugin() {
    DebugSpewAlways("MQ2SidAssist::Shutting down");

    // Examples:
    // RemoveCommand("/mycommand");
    // RemoveXMLFile("MQUI_MyXMLFile.xml");
    // RemoveMQ2Data("mytlo");
}

PLUGIN_API void OnPulse() {
    if (GetGameState() != GAMESTATE_INGAME) {
        return;
    }
    static std::chrono::steady_clock::time_point PulseTimer = std::chrono::steady_clock::now();
    if (std::chrono::steady_clock::now() > PulseTimer) {
        PulseTimer = std::chrono::steady_clock::now() + std::chrono::milliseconds(100);
        SidAssist::getInstance().OnPulse();
    }
}

/**
 * @fn OnIncomingChat
 *
 * This is called each time a line of chat is shown.  It occurs after MQ filters
 * and chat events have been handled.  If you need to know when MQ2 has sent chat,
 * consider using @ref OnWriteChatColor instead.
 *
 * For a list of Color values, see the constants for USERCOLOR_. The default is
 * USERCOLOR_DEFAULT.
 *
 * @param Line const char* - The line of text that was shown
 * @param Color int - The type of chat text this was sent as
 *
 * @return bool - Whether to filter this chat from display
 */
PLUGIN_API bool OnIncomingChat(const char *Line, DWORD Color) {
    // DebugSpewAlways("MQ2SidAssist::OnIncomingChat(%s, %d)", Line, Color);
    return false;
}

/**
 * @fn OnAddSpawn
 *
 * This is called each time a spawn is added to a zone (ie, something spawns). It is
 * also called for each existing spawn when a plugin first initializes.
 *
 * When zoning, this is called for all spawns in the zone after @ref OnEndZone is
 * called and before @ref OnZoned is called.
 *
 * @param pNewSpawn PSPAWNINFO - The spawn that was added
 */
PLUGIN_API void OnAddSpawn(PSPAWNINFO pNewSpawn) {
    // DebugSpewAlways("MQ2SidAssist::OnAddSpawn(%s)", pNewSpawn->Name);
}

/**
 * @fn OnRemoveSpawn
 *
 * This is called each time a spawn is removed from a zone (ie, something despawns
 * or is killed).  It is NOT called when a plugin shuts down.
 *
 * When zoning, this is called for all spawns in the zone after @ref OnBeginZone is
 * called.
 *
 * @param pSpawn PSPAWNINFO - The spawn that was removed
 */
PLUGIN_API void OnRemoveSpawn(PSPAWNINFO pSpawn) {
    // DebugSpewAlways("MQ2SidAssist::OnRemoveSpawn(%s)", pSpawn->Name);
}

/**
 * @fn OnAddGroundItem
 *
 * This is called each time a ground item is added to a zone (ie, something spawns).
 * It is also called for each existing ground item when a plugin first initializes.
 *
 * When zoning, this is called for all ground items in the zone after @ref OnEndZone
 * is called and before @ref OnZoned is called.
 *
 * @param pNewGroundItem PGROUNDITEM - The ground item that was added
 */
PLUGIN_API void OnAddGroundItem(PGROUNDITEM pNewGroundItem) {
    // DebugSpewAlways("MQ2SidAssist::OnAddGroundItem(%d)", pNewGroundItem->DropID);
}

/**
 * @fn OnRemoveGroundItem
 *
 * This is called each time a ground item is removed from a zone (ie, something
 * despawns or is picked up).  It is NOT called when a plugin shuts down.
 *
 * When zoning, this is called for all ground items in the zone after
 * @ref OnBeginZone is called.
 *
 * @param pGroundItem PGROUNDITEM - The ground item that was removed
 */
PLUGIN_API void OnRemoveGroundItem(PGROUNDITEM pGroundItem) {
    // DebugSpewAlways("MQ2SidAssist::OnRemoveGroundItem(%d)", pGroundItem->DropID);
}

/**
 * @fn OnBeginZone
 *
 * This is called just after entering a zone line and as the loading screen appears.
 */
PLUGIN_API void OnBeginZone() {
    // DebugSpewAlways("MQ2SidAssist::OnBeginZone()");
}

/**
 * @fn OnEndZone
 *
 * This is called just after the loading screen, but prior to the zone being fully
 * loaded.
 *
 * This should occur before @ref OnAddSpawn and @ref OnAddGroundItem are called. It
 * always occurs before @ref OnZoned is called.
 */
PLUGIN_API void OnEndZone() {
    // DebugSpewAlways("MQ2SidAssist::OnEndZone()");
}

/**
 * @fn OnZoned
 *
 * This is called after entering a new zone and the zone is considered "loaded."
 *
 * It occurs after @ref OnEndZone @ref OnAddSpawn and @ref OnAddGroundItem have
 * been called.
 */
PLUGIN_API void OnZoned() {
    // DebugSpewAlways("MQ2SidAssist::OnZoned()");
}

/**
 * @fn OnUpdateImGui
 *
 * This is called each time that the ImGui Overlay is rendered. Use this to render
 * and update plugin specific widgets.
 *
 * Because this happens extremely frequently, it is recommended to move any actual
 * work to a separate call and use this only for updating the display.
 */
PLUGIN_API void OnUpdateImGui() {
/*
	if (GetGameState() == GAMESTATE_INGAME)
	{
		static bool ShowMQ2SidAssistWindow = true;
		ImGui::Begin("MQ2SidAssist", &ShowMQ2SidAssistWindow, ImGuiWindowFlags_MenuBar);
		if (ImGui::BeginMenuBar())
		{
			ImGui::Text("MQ2SidAssist is loaded!");
			ImGui::EndMenuBar();
		}
		ImGui::End();
	}
*/
}

/**
 * @fn OnMacroStart
 *
 * This is called each time a macro starts (ex: /mac somemacro.mac), prior to
 * launching the macro.
 *
 * @param Name const char* - The name of the macro that was launched
 */
PLUGIN_API void OnMacroStart(const char *Name) {
    // DebugSpewAlways("MQ2SidAssist::OnMacroStart(%s)", Name);
}

/**
 * @fn OnMacroStop
 *
 * This is called each time a macro stops (ex: /endmac), after the macro has ended.
 *
 * @param Name const char* - The name of the macro that was stopped.
 */
PLUGIN_API void OnMacroStop(const char *Name) {
    // DebugSpewAlways("MQ2SidAssist::OnMacroStop(%s)", Name);
}

/**
 * @fn OnLoadPlugin
 *
 * This is called each time a plugin is loaded (ex: /plugin someplugin), after the
 * plugin has been loaded and any associated -AutoExec.cfg file has been launched.
 * This means it will be executed after the plugin's @ref InitializePlugin callback.
 *
 * This is also called when THIS plugin is loaded, but initialization tasks should
 * still be done in @ref InitializePlugin.
 *
 * @param Name const char* - The name of the plugin that was loaded
 */
PLUGIN_API void OnLoadPlugin(const char *Name) {
    // DebugSpewAlways("MQ2SidAssist::OnLoadPlugin(%s)", Name);
}

/**
 * @fn OnUnloadPlugin
 *
 * This is called each time a plugin is unloaded (ex: /plugin someplugin unload),
 * just prior to the plugin unloading.  This means it will be executed prior to that
 * plugin's @ref ShutdownPlugin callback.
 *
 * This is also called when THIS plugin is unloaded, but shutdown tasks should still
 * be done in @ref ShutdownPlugin.
 *
 * @param Name const char* - The name of the plugin that is to be unloaded
 */
PLUGIN_API void OnUnloadPlugin(const char *Name) {
    // DebugSpewAlways("MQ2SidAssist::OnUnloadPlugin(%s)", Name);
}
