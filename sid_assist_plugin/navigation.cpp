#include "navigation.h"

#include <cmath>
#include "sid_common.h"
#include "mq2nav_api.h"

std::shared_ptr<Loc> getLocation(PSPAWNINFO pSpawn) {
    if (!pSpawn) {
        logger.errorf("Tried to get location for NULL. This is an error, please report.");
        return nullptr;
    }
    return std::make_shared<Loc>(pSpawn->X, pSpawn->Y, pSpawn->Z);
}

double getDistance(PSPAWNINFO pSpawn) {
    return getLocation((PSPAWNINFO) pLocalPlayer)->distanceTo(getLocation(pSpawn));
}

void Navigator::SetModeManual() {
    this->mode = manual;
    this->campLocation = nullptr;

    if (isNavPathActive()) {
        runCommandf("/squelch /nav stop");
    }

    logger.infof("Navigation set to \ayMANUAL\ax.");
}

void Navigator::SetCamp(float x, float y, float z) {
    this->campLocation = std::make_unique<Loc>(x, y, z);
    this->mode = camp;
    logger.infof("Navigation set to \ayCAMP\ax at " LOC_FMT ".", x, y, z);
}

void Navigator::SetModeChase() {
    this->mode = chase;
    this->campLocation = nullptr;
    logger.infof("Navigation set to \ayCHASE\ax.");
}


std::unique_ptr<Task> Navigator::GetTask() {
    if (mode == camp && GetDistanceToCamp() > 50) {
        logger.debugf("We're too far from camp. Trying to return");
        return std::make_unique<ReturnToCampTask>();
    } else if (mode == chase && GetDistanceToChaseTarget() > 50) {
        logger.debugf("We're too far from our Chase Target. Chasing.");
        return std::make_unique<NavigateToSpawnTask>(getGroupMainAssist());
    }
    return nullptr;
}

double Navigator::GetDistanceToCamp() const {
    if (!campLocation) {
        return 0.0F;
    }
    return campLocation->distanceTo(getLocation((PSPAWNINFO) pLocalPlayer));
}

void Navigator::ReturnToCamp() const {
    logger.infof("Running to camp at " LOC_FMT ".", campLocation->x, campLocation->y, campLocation->z);
    runCommandf("/squelch /nav locxyz %f %f %f log=off", campLocation->x, campLocation->y, campLocation->z);
}

double Navigator::GetDistanceToChaseTarget() {
    return getDistance(getGroupMainAssist());
}

Loc::Loc(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
};

double Loc::distanceTo(const std::shared_ptr<Loc> &that) const {
    return sqrt(abs(pow(this->x - that->x, 2) + pow(this->y - that->y, 2) + pow(this->z - that->z, 2)));
}

bool ReturnToCampTask::IsDone() const {
    bool done = Navigator::GetInstance().GetDistanceToCamp() < 12;
    if (done) {
        logger.debugf("Looks like we are where we want to be.");
    }
    return done;
}

void ReturnToCampTask::Cleanup() {
    runCommandf("/squelch /nav stop log=off %s");
}

void ReturnToCampTask::DoWork() {
    if (!gbMoving && !IsDone() && !isNavPathActive()) {
        Navigator::GetInstance().ReturnToCamp();
    }
}

NavigateToSpawnTask::NavigateToSpawnTask(PSPAWNINFO target) {
    this->target = target;
}

bool NavigateToSpawnTask::IsDone() const {
    return getDistance(getGroupMainAssist()) < 15;
}

void NavigateToSpawnTask::DoWork() {
    if (!gbMoving && !IsDone() && !isNavPathActive()) {
        logger.debugf("Navigating to " PC_NAME_FMT, target->Name);
        runCommandf("/squelch /nav id %d log=off", target->SpawnID);
    }
}

void NavigateToSpawnTask::Cleanup() {
    runCommandf("/squelch /nav stop log=off");
}
