#include "sid_common.h"
#include <cstring>

char *getPlayerClassAbbr() {
    return ClassInfo[GetCharInfo2()->Class].ShortName;
}

void runCommandf(const char *szFormat, ...) {
    va_list vaList;
    va_start(vaList, szFormat);
    int len = _vscprintf(szFormat, vaList) + 1;
    if (char *szOutput = (char *) LocalAlloc(LPTR, len + 32)) {
        vsprintf_s(szOutput, len, szFormat, vaList);
        EzCommand(szOutput);
        LocalFree(szOutput);
    }
}

// Copied/adapted from MQ2Status.cpp:947
// and FarmTest/NavCommands.h
PMQPLUGIN findPlugin(const char *pluginName) {
    PMQPLUGIN pPlugin = pPlugins;
    while (pPlugin) {
        if (!_stricmp(pluginName, pPlugin->szFilename)) {
            return pPlugin;
        }
        pPlugin = pPlugin->pNext;
    }
    return nullptr;
}

bool isPluginLoaded(const char *pluginName) {
    if (findPlugin(pluginName)) {
        return true;
    }
    return false;
}

PSPAWNINFO getGroupMainAssist() {
    for (auto pGroupMember : GetCharInfo()->pGroupInfo->pMember) {
        if (!pGroupMember) {
            continue;
        }
        if (!pGroupMember->pSpawn) {
            continue;
        }

        if (pGroupMember->MainAssist) {
            return pGroupMember->pSpawn;
        }
    }
    return nullptr;
}

std::unique_ptr<std::vector<std::string>> getArgs(std::string szLine, const std::string &delimiter) {
    auto args = std::make_unique<std::vector<std::string>>();
    size_t pos = 0;
    std::string token;
    while ((pos = szLine.find(delimiter)) != std::string::npos) {
        token = szLine.substr(0, pos);
        if (!token.empty()) {
            args->push_back(token);
        }
        szLine.erase(0, pos + delimiter.length());
    }
    if (!szLine.empty()) {
        args->push_back(szLine);
    }
    return args;
}

bool iStrEquals(const char *arg1, const std::string &arg2) {
    return _strcmpi(arg1, arg2.c_str()) == 0;
}

bool parseBoolArg(std::string arg) {
    if (iStrEquals("on", arg) || iStrEquals("true", arg) || iStrEquals("yes", arg) || iStrEquals("y", arg) ||
        iStrEquals("1", arg)) {
        return true;
    } else if (iStrEquals("off", arg) || iStrEquals("false", arg) || iStrEquals("no", arg) || iStrEquals("n", arg) ||
               iStrEquals("0", arg)) {
        return false;
    }
    throw std::invalid_argument("Invalid argument. Valid values are: [on, off, true, false, yes, no, 1, 0]");
}
