#include "mq2nav_api.h"
#include "sid_common.h"
#include "Logger.h"

template<class T>
T callExportedFunction0(char *FnName, T defaultValue) {
    PMQPLUGIN mq2Nav = findPlugin("MQ2Nav");
    if (!mq2Nav) {
        return defaultValue;
    }

    typedef T (*fnType)();
    auto fn = (fnType) GetProcAddress(mq2Nav->hModule, FnName);
    if (!fn) {
        return defaultValue;
    }
    return fn();
}

bool isNavPathActive() {
    return callExportedFunction0<bool>("IsNavPathActive", false);
}

bool isNavMeshLoaded() {
    return callExportedFunction0<bool>("IsNavMeshLoaded", false);
}

float getNavPathLength() {
    return callExportedFunction0<float>("GetNavPathLength", -1.0F);
}
