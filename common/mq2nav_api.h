#pragma once

bool isNavPathActive();
bool isNavMeshLoaded();
float getNavPathLength();
