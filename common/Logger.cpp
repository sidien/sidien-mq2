#include "Logger.h"

#include <cstring>
#include <mq/Plugin.h>

Logger::Logger(const char *loggerName) {
    this->LoggerName = new char[strlen(loggerName + 1)];
    strcpy_s(this->LoggerName, strlen(loggerName) + 1, loggerName);
    int headerLength = strlen(loggerName) + 40; // randomly picked 40
    this->ErrorPrefix = new char[headerLength];
    sprintf_s(this->ErrorPrefix, headerLength, "\a-t[\at%s\ax] \a-r[\arERROR\ax] \ao", loggerName);
    this->InfoPrefix = new char[headerLength];
    sprintf_s(this->InfoPrefix, headerLength, "\a-t[\at%s\ax] \ao", loggerName);
    this->DebugPrefix = new char[headerLength];
    sprintf_s(this->DebugPrefix, headerLength, "\a-t[\at%s\ax] \a-y[\ayDEBUG\a-y] \ao", loggerName);
}

Logger::~Logger() {
    delete this->LoggerName;
    delete this->InfoPrefix;
    delete this->DebugPrefix;
}

void DoLog(const char *szPrefix, const char *szFormat, va_list vaList) {
    unsigned int formatSize = strlen(szPrefix) + strlen(szFormat) + 1;
    char *formatWithHeader = new char[formatSize];
    sprintf_s(formatWithHeader, formatSize, "%s%s", szPrefix, szFormat);
    int len = _vscprintf(formatWithHeader, vaList) + 1;// _vscprintf doesn't count terminating '\0'
    if (char *szOutput = (char *) LocalAlloc(LPTR, len + 32)) {
        vsprintf_s(szOutput, len, formatWithHeader, vaList);
        WriteChatColor(szOutput);
        LocalFree(szOutput);
    }
    delete[] formatWithHeader;
}

void Logger::errorf(const char *szFormat, ...) const {
    va_list vaList;
    va_start(vaList, szFormat);
    DoLog(ErrorPrefix, szFormat, vaList);
}

void Logger::infof(const char *szFormat, ...) const {
    va_list vaList;
    va_start(vaList, szFormat);
    DoLog(InfoPrefix, szFormat, vaList);
}

void Logger::debugf(const char *szFormat, ...) const {
    if (!debugEnabled) {
        return;
    }
    va_list vaList;
    va_start(vaList, szFormat);
    DoLog(DebugPrefix, szFormat, vaList);
}

void Logger::setDebugEnabled(const bool debugEnabled) {
    this->debugEnabled = debugEnabled;
}

bool Logger::isDebugEnabled() const {
    return this->debugEnabled;
}
