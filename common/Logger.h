#pragma once

#define LOC_FMT "(\a-gx\ax: \ag%.0f\ax, \a-gy\ax: \ag%.0f\ax, \a-gz\ax: \ag%.0f\ax)"
#define PC_NAME_FMT "\am%s\ax"
#define NPC_NAME_FMT "\a-t%s\ax"
#define SPELL_NAME_FMT "\ap%s\ax"

class Logger {
public:
    explicit Logger(const char *loggerName);
    ~Logger();
    void errorf(const char *szFormat, ...) const;
    void infof(const char *szFormat, ...) const;
    void debugf(const char *szFormat, ...) const;
    void setDebugEnabled(bool debugEnabled);
    bool isDebugEnabled() const;
private:
    bool debugEnabled = false;
    char *LoggerName;
    char *ErrorPrefix;
    char *InfoPrefix;
    char *DebugPrefix;
};
