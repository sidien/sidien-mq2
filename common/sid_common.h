#pragma once

#include <mq/Plugin.h>

void runCommandf(const char *szFormat, ...);
char *getPlayerClassAbbr();
PMQPLUGIN findPlugin(const char *pluginName);
bool isPluginLoaded(const char *pluginName);
PSPAWNINFO getGroupMainAssist();

std::unique_ptr<std::vector<std::string>> getArgs(std::string szLine, const std::string &delimiter = " ");
bool iStrEquals(const char *arg1, const std::string &arg2);
bool parseBoolArg(std::string arg);
